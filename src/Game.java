import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player('O');
		x = new Player('X');
		

	}

	public void play() {
		while(true) {
			playOne();
		}
	}
	
	public void playOne() {
		board = new Board(x, o);
		printWelcome();
		while (true) {
			printBorad();
			printTurn();
			input();
			if(board.isFinish()) {
				break;
			}
			board.switchTurn();
		}
		printBorad();
		printWinner();
		printStat();
	}
	
	private void printStat(){
		 System.out.println(x.getName() + " W,D,L:" + x.getWin() +" "+ x.getLose() +" "+ x.getDraw()  );
		 System.out.println(o.getName() + " W,D,L:" + o.getWin() +" "+ o.getLose() +" "+ o.getDraw()  );
	}
	
	private void printWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName()+ " win....");
	}

	private void printWelcome() {
		System.out.println("Welcome to OX Game ");
	}

	private void printBorad() {
		char[][] table = board.getTable();
		System.out.println(" 1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void printTurn() {
		Player player = board.getCurrent();
		System.out.println(player.getName() + " turn..");
	}

	private void input() {
		while (true) {
			try {
				Scanner kb = new Scanner(System.in);

				System.out.println("Please input Row Col: ");
				String input = kb.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Error to input try again");
					continue;
				}
				int row = Integer.parseInt(str[0]) - 1;
				int col = Integer.parseInt(str[1]) - 1;
				if (board.setTable(row, col) == false) {
					System.out.println("Table is not empty!!");
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println("Error to input try again");
				continue;
			}
		}

	}
}
